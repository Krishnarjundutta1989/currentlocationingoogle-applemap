//
//  ViewController.h
//  LocatonSearchInMap
//
//  Created by click labs 115 on 10/14/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDataSource,MKMapViewDelegate,CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;
@property (strong, nonatomic) IBOutlet UITableView *searchTable;

@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;

@end

